/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_input.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/10 15:06:13 by tfriedri          #+#    #+#             */
/*   Updated: 2022/06/14 11:11:31 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

int	check_for_duplicates(t_list *stack, int value)
{
	while (stack)
	{
		if (value == *(int *)(stack->content))
			return (1);
		stack = stack->next;
	}
	return (0);
}

int	check_argv_for_chars(char *argv)
{
	size_t	i;
	int		ret_val;
	int		num_bool;

	ret_val = 0;
	num_bool = 0;
	i = 0;
	while (i++ < ft_strlen(argv))
	{
		if ((ft_isdigit(argv[i - 1] == 0)) && (argv[i - 1] != '-')
			&& (argv[i - 1] != '+'))
			ret_val = 1;
		if ((ft_isdigit(argv[i - 1]) == 0) && (num_bool == 1 || i != 1))
			ret_val = 1;
		if (ft_isdigit(argv[i - 1]) == 1)
			num_bool = 1;
	}
	if (num_bool != 1)
		ret_val = 1;
	return (ret_val);
}

int	string_to_list(char	**array, t_list **stack_a)
{
	int				i;
	long long int	*tmp;

	i = 0;
	while (array[i])
	{
		tmp = malloc(sizeof(int));
		if (!tmp)
			return (0);
		*tmp = ft_long_long_atoi(array[i]);
		if (check_argv_for_chars(array[i]) == 1
			|| check_for_duplicates(*stack_a, *tmp) == 1
			|| *tmp < -2147483648 || *tmp > 2147483647)
		{
			ft_putstr_fd("Error\n", 2);
			free_stack(*stack_a);
			return (0);
		}
		ft_lstadd_back(stack_a, ft_lstnew(tmp));
		free(array[i]);
		i++;
	}
	return (1);
}

int	get_stack_a(t_list **stack_a, int argc, char *argv[])
{
	int		i;
	char	*str;
	char	*free_str;
	char	**array;

	i = 1;
	str = argv[i];
	while (++i < argc)
	{
		str = ft_strjoin(str, " ");
		if (i != 2)
			free(free_str);
		free_str = str;
		str = ft_strjoin(str, argv[i]);
		free(free_str);
		free_str = str;
	}
	array = ft_split(str, ' ');
	free(free_str);
	if (string_to_list(array, stack_a) == 0)
		return (0);
	free(array);
	return (1);
}
