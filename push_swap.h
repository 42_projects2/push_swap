/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 17:26:34 by tfriedri          #+#    #+#             */
/*   Updated: 2022/06/14 11:04:33 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "./libft/libft.h"

int					main(int argc, char *argv[]);
int					only_space(char *str);
void				free_stack(t_list *stack);
int					check_for_duplicates(t_list *stack, int value);
int					check_argv_for_chars(char *argv);
int					string_to_list(char	**array, t_list **stack_a);
int					get_stack_a(t_list **stack_a, int argc, char *argv[]);
long long int		ft_long_long_atoi(const char *str);
void				push_a(t_list **stack_a, t_list **stack_b);
void				push_b(t_list **stack_a, t_list **stack_b);
void				rotate(t_list **stack, char *str);
void				rotate_both(t_list **stack_a, t_list **stack_b);
void				rev_rotate(t_list **stack, char *str);
void				rev_rotate_both(t_list **stack_a, t_list **stack_b);
void				swap(t_list **stack, char *str);
void				swap_both(t_list **stack_a, t_list **stack_b);
void				only_three(t_list **stack_a);
int					get_div_fact(int lstsz);
void				sort_alg(t_list **stack_a, t_list **stack_b);
void				sort_main(t_list **stack_a, t_list **stack_b);
int					stack_nr(t_list *stack);
int					get_top_num(t_list *stack, int lstsz, int div_fact);
int					high_apart_x(t_list *stack, int x);
int					get_next_possible(t_list *stack, int t_nr);
void				rotate_to_top(t_list **stack, int num, char a_or_b);
int					check_stack_order(t_list *stack);
int					check_rev_stack_order(t_list *stack);
int					get_num_pos(t_list *stack_a, int num);
int					get_lowest(t_list *stack);
int					get_highest(t_list *stack);
int					get_lowest_apart_x(t_list *stack, int x);
void				num_to_top(t_list **stack, int num, char a_or_b);
void				do_rr(t_list **s_a, int nr_a, t_list **s_b, int nr_b);
void				a_to_b(t_list **s_a, t_list **s_b, int top_num);

#endif
