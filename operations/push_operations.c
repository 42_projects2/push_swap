/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_operations.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/09 15:55:11 by tfriedri          #+#    #+#             */
/*   Updated: 2022/05/24 11:49:17 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void	push_a(t_list **stack_a, t_list **stack_b)
{
	int		*tmp;
	t_list	*new_stack_b;

	tmp = (int *)((*stack_b)->content);
	ft_lstadd_front(stack_a, ft_lstnew(tmp));
	new_stack_b = (*stack_b)->next;
	free(*stack_b);
	*stack_b = new_stack_b;
	ft_putstr_fd("pa\n", 1);
}

void	push_b(t_list **stack_a, t_list **stack_b)
{
	int		*tmp;
	t_list	*new_stack_a;

	tmp = (int *)((*stack_a)->content);
	ft_lstadd_front(stack_b, ft_lstnew(tmp));
	new_stack_a = (*stack_a)->next;
	free(*stack_a);
	*stack_a = new_stack_a;
	ft_putstr_fd("pb\n", 1);
}
