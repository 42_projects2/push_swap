/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap_operations.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/09 15:24:58 by tfriedri          #+#    #+#             */
/*   Updated: 2022/05/24 11:59:30 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void	swap(t_list **stack, char *str)
{
	int	*tmp;

	tmp = (int *)((*stack)->content);
	(*stack)->content = (*stack)->next->content;
	(*stack)->next->content = tmp;
	ft_putstr_fd(str, 1);
}

void	swap_both(t_list **stack_a, t_list **stack_b)
{
	swap(stack_a, "");
	swap(stack_b, "");
	ft_putstr_fd("ss\n", 1);
}
