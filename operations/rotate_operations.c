/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate_operations.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/09 16:50:12 by tfriedri          #+#    #+#             */
/*   Updated: 2022/05/24 11:50:28 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void	rotate(t_list **stack, char *str)
{
	int		*tmp;
	t_list	*new_stack;

	tmp = (int *)((*stack)->content);
	ft_lstadd_back(stack, ft_lstnew(tmp));
	new_stack = (*stack)->next;
	free(*stack);
	*stack = new_stack;
	ft_putstr_fd(str, 1);
}

void	rotate_both(t_list **stack_a, t_list **stack_b)
{
	rotate(stack_a, "");
	rotate(stack_b, "");
	ft_putstr_fd("rr\n", 1);
}

void	rev_rotate(t_list **stack, char *str)
{
	int		*tmp;
	t_list	*last;
	t_list	*lst;

	tmp = (int *)ft_lstlast(*stack)->content;
	ft_lstadd_front(stack, ft_lstnew(tmp));
	last = ft_lstlast(*stack);
	lst = *stack;
	while (lst)
	{
		if (!(lst->next->next))
			break ;
		lst = lst->next;
	}
	lst->next = NULL;
	free(last);
	ft_putstr_fd(str, 1);
}

void	rev_rotate_both(t_list **stack_a, t_list **stack_b)
{
	rev_rotate(stack_a, "");
	rev_rotate(stack_b, "");
	ft_putstr_fd("rrr\n", 1);
}
