# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/05/04 17:11:14 by tfriedri          #+#    #+#              #
#    Updated: 2022/06/14 11:13:19 by tfriedri         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = push_swap
LIBFTNAME = libft.a
CC = cc
CFLAGS = -Wall -Werror -Wextra
LIBFTDIR = ./libft
LIBFT= ./libft/libft.a

SRCS =	main.c \
		get_input/get_input.c \
		get_input/ft_long_long_atoi.c \
		operations/swap_operations.c \
		operations/push_operations.c \
		operations/rotate_operations.c \
		sort_algs/sort_main.c \
		sort_algs/sort_general_1.c \
		sort_algs/sort_general_2.c \
		sort_algs/sort_general_3.c \

OBJS = $(SRCS:.c=.o)

%.o : %.c
	@$(CC) $(CFLAGS) -c $< -o $@

all: $(NAME)

$(LIBFT):
	@make bonus -C $(LIBFTDIR)

$(NAME): $(LIBFT) $(OBJS)
	@$(CC) $(CFLAGS) $(OBJS) -L$(LIBFTDIR) -lft -o $(NAME)

clean:
	@rm -f $(OBJS)
	@cd $(LIBFTDIR) && make clean
	
fclean: clean
	@rm -f $(NAME)
	@cd $(LIBFTDIR) && make fclean

re: fclean all