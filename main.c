/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/04 17:29:01 by tfriedri          #+#    #+#             */
/*   Updated: 2022/06/14 11:03:43 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	free_stack(t_list *stack)
{
	if (stack == NULL)
		return ;
	while (stack)
	{
		free(stack->content);
		stack = stack->next;
	}
}

int	only_space(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (str[i] != ' ')
			return (0);
		i++;
	}
	return (1);
}

int	main(int argc, char *argv[])
{
	int		i;
	t_list	*stack_a;
	t_list	*stack_b;

	stack_a = NULL;
	stack_b = NULL;
	i = 1;
	while (i < argc)
	{
		if (ft_strlen(argv[i]) == 0 || only_space(argv[i]) == 1)
		{
			ft_putstr_fd("Error\n", 2);
			return (0);
		}
		i++;
	}
	if ((argc <= 1) || get_stack_a(&stack_a, argc, argv) == 0)
		return (0);
	sort_main(&stack_a, &stack_b);
	return (0);
}
