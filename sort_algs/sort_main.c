/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_main.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/10 09:42:34 by tfriedri          #+#    #+#             */
/*   Updated: 2022/06/13 18:14:02 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

void	only_three(t_list **stack_a)
{
	int	low;
	int	high;

	low = get_lowest(*stack_a);
	high = get_highest(*stack_a);
	if (check_rev_stack_order(*stack_a) == 1)
	{
		swap(stack_a, "sa\n");
		rev_rotate(stack_a, "rra\n");
	}
	if (*(int *)(*stack_a)->content == low
		&& *(int *)(*stack_a)->next->content == high)
	{
		swap(stack_a, "sa\n");
		rotate(stack_a, "ra\n");
	}
	if (*(int *)(*stack_a)->content == high
		&& *(int *)(*stack_a)->next->content == low)
		rotate(stack_a, "ra\n");
	if (*(int *)(*stack_a)->next->content == low
		&& *(int *)(*stack_a)->next->next->content == high)
		swap(stack_a, "sa\n");
	if (*(int *)(*stack_a)->next->content == high
		&& *(int *)(*stack_a)->next->next->content == low)
		rev_rotate(stack_a, "rra\n");
}

int	get_div_fact(int lstsz)
{
	if (lstsz > 8 && lstsz <= 10)
		return (2);
	if (lstsz <= 50)
		return (4);
	if (lstsz <= 75)
		return (5);
	if (lstsz <= 100)
		return (6);
	if (lstsz <= 150)
		return (lstsz * 0.07);
	if (lstsz <= 200)
		return (lstsz * 0.06);
	if (lstsz <= 300)
		return (lstsz * 0.05);
	if (lstsz <= 400)
		return (lstsz * 0.04);
	return (lstsz * 0.05);
}

void	sort_alg(t_list **stack_a, t_list **stack_b)
{
	while (*stack_a && !((check_stack_order(*stack_a) == 1
				|| ft_lstsize(*stack_a) == 3)
			&& get_lowest(*stack_a) > get_highest(*stack_b)))
		a_to_b(stack_a, stack_b, get_top_num(*stack_a,
				ft_lstsize(*stack_a), get_div_fact(ft_lstsize(*stack_a))));
	if (ft_lstsize(*stack_a) == 3)
		only_three(stack_a);
	num_to_top(stack_b, get_highest(*stack_b), 'b');
	while (ft_lstsize(*stack_b) != 0)
		push_a(stack_a, stack_b);
}

void	sort_main(t_list **stack_a, t_list **stack_b)
{
	int	lstsz;

	lstsz = ft_lstsize(*stack_a);
	if (lstsz == 0)
		exit(0);
	if (check_stack_order(*stack_a) == 1)
		return ;
	else if (lstsz == 2)
		ft_putstr_fd("sa\n", 1);
	else if (lstsz == 3)
		only_three(stack_a);
	else
		sort_alg(stack_a, stack_b);
}
