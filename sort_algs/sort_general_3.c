/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_general_3.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/07 13:57:29 by tfriedri          #+#    #+#             */
/*   Updated: 2022/06/07 14:08:37 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

int	get_lowest_apart_x(t_list *stack, int x)
{
	int	nextlow;

	nextlow = 2147483647;
	while (stack)
	{
		if (*(int *)stack->content < nextlow && *(int *)stack->content > x)
			nextlow = *(int *)stack->content;
		stack = stack->next;
	}
	return (nextlow);
}

void	num_to_top(t_list **stack, int num, char a_or_b)
{
	int	num_pos;

	num_pos = get_num_pos(*stack, num);
	if (num_pos <= (ft_lstsize(*stack) / 2))
	{
		while (num_pos-- > 0)
		{
			rotate(stack, "r\0");
			ft_putchar_fd(a_or_b, 1);
			ft_putstr_fd("\n\0", 1);
		}
	}
	else
	{
		while (num_pos++ < ft_lstsize(*stack))
		{
			rev_rotate(stack, "rr\0");
			ft_putchar_fd(a_or_b, 1);
			ft_putstr_fd("\n\0", 1);
		}
	}
}

void	do_rr(t_list **s_a, int num_a, t_list **s_b, int num_b)
{
	int	rr_count;
	int	nr_pos_a;
	int	nr_pos_b;

	rr_count = 0;
	nr_pos_a = get_num_pos(*s_a, num_a);
	nr_pos_b = get_num_pos(*s_b, num_b);
	if (nr_pos_a <= ft_lstsize(*s_a) / 2 && nr_pos_b <= ft_lstsize(*s_b) / 2)
	{
		if (nr_pos_a <= nr_pos_b)
			rr_count = nr_pos_a;
		else
			rr_count = nr_pos_b;
	}
	while (rr_count-- > 0)
		rotate_both(s_a, s_b);
}

void	a_to_b(t_list **s_a, t_list **s_b, int top_num)
{
	int	next_poss_a;
	int	next_b;

	next_poss_a = get_next_possible(*s_a, top_num);
	if (*s_b)
	{
		if (next_poss_a > get_highest(*s_b)
			|| next_poss_a < get_lowest(*s_b))
			next_b = get_highest(*s_b);
		else
			next_b = high_apart_x(*s_b, next_poss_a);
		do_rr(s_a, next_poss_a, s_b, next_b);
		num_to_top(s_b, next_b, 'b');
	}
	if (next_poss_a == get_lowest(*s_a))
		num_to_top(s_a, next_poss_a, 'a');
	else
		rotate_to_top(s_a, next_poss_a, 'a');
	push_b(s_a, s_b);
}
