/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_general.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: marvin <marvin@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/11 11:59:47 by marvin            #+#    #+#             */
/*   Updated: 2022/05/11 11:59:47 by marvin           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

int	check_stack_order(t_list *stack)
{
	int	tmp;
	int	new_tmp;

	new_tmp = *(int *)(stack->content);
	stack = stack->next;
	while (stack)
	{
		tmp = new_tmp;
		new_tmp = *(int *)(stack->content);
		stack = stack->next;
		if (new_tmp < tmp)
			return (0);
	}
	return (1);
}

int	check_rev_stack_order(t_list *stack)
{
	int	tmp;
	int	new_tmp;

	new_tmp = *(int *)(stack->content);
	stack = stack->next;
	while (stack)
	{
		tmp = new_tmp;
		new_tmp = *(int *)(stack->content);
		stack = stack->next;
		if (new_tmp > tmp)
			return (0);
	}
	return (1);
}

int	get_num_pos(t_list *stack_a, int num)
{
	int	i;

	i = 0;
	while (stack_a)
	{
		if (*(int *)stack_a->content == num)
			return (i);
		stack_a = stack_a->next;
		i++;
	}
	return (i);
}

int	get_lowest(t_list *stack)
{
	int	low;

	low = *(int *)(stack->content);
	while (stack)
	{
		if (*(int *)stack->content < low)
			low = *(int *)stack->content;
		stack = stack->next;
	}
	return (low);
}

int	get_highest(t_list *stack)
{
	int	high;

	high = *(int *)(stack->content);
	while (stack)
	{
		if (*(int *)stack->content > high)
			high = *(int *)stack->content;
		stack = stack->next;
	}
	return (high);
}
