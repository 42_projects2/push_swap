/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_alg_3.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/18 20:07:58 by tfriedri          #+#    #+#             */
/*   Updated: 2022/05/18 20:07:58 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../push_swap.h"

int	stack_nr(t_list *stack)
{
	return (*(int *)(stack->content));
}

int	get_top_num(t_list *stack, int lstsz, int div_fact)
{
	int	i;
	int	low;

	i = 0;
	low = get_lowest(stack);
	while (++i < (lstsz / div_fact))
		low = get_lowest_apart_x(stack, low);
	return (low);
}

int	high_apart_x(t_list *stack, int x)
{
	int	nexthigh;

	nexthigh = -2147483648;
	while (stack)
	{
		if (*(int *)stack->content > nexthigh && stack_nr(stack) < x)
			nexthigh = stack_nr(stack);
		stack = stack->next;
	}
	return (nexthigh);
}

int	get_next_possible(t_list *stack, int t_nr)
{
	while (stack)
	{
		if (stack_nr(stack) <= t_nr)
			return (stack_nr(stack));
		stack = stack->next;
	}
	return (t_nr);
}

void	rotate_to_top(t_list **stack, int num, char a_or_b)
{
	while (stack_nr(*stack) != num)
	{
		rotate(stack, "r\0");
		ft_putchar_fd(a_or_b, 1);
		ft_putstr_fd("\n\0", 1);
	}
}
